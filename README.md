# Weather API (INSHUR Task)

### Description ###

REST API that use Open Weather API to provide the warmest day over the next 7 days:

* endpoint -> http://localhost:8081/weather-api/getWarmestDayNextSevenDays/{lat}/{lon} -> lat(latitude), lon(longitude)
* method type -> GET
* response -> WarmestDay  
	{
    "lat": float,  
    "lon": float,  
    "timeZone": string,  
    "date": string,  
    "kelvinTemperature": string,  
    "celsiusTemperature": string,  
    "humidity": string  
	}
* package structure
	com.inshur.openweather  
		- controller -> Rest Controller Layer  
		- core -> Service core to invoke Open Weather API  
		- service -> Service Layer  
		- util -> Enum, utilities to manipulate data  
* application.properties
	- openweather.api.key -> API Key required to use Open Weather API  
	- openweather.url -> Open Weather API URL  
	- server.port -> Tomcat port  
* JUnit Test Cases
	- com.inshur.openweather.OpenWeatherIntegrationApplicationTests  

### Requirements ###

* Maven
* Eclipse / STS (Spring Tool Suite)
* Java 16

### How to build ###

Using Eclipse/STS:

* Import project -> Maven Project
* Setup a new Maven configuration -> Run Configuration -> New Maven Configuration -> Maven goals -> clean install
* Run the new Maven configutarion

### How to run ###

Using Eclipse/STS:

* Setup new Spring Boot Configuration -> Main class -> com.inshur.openweather.OpenWeatherIntegrationApplication
* Run new Spring Boot Application
