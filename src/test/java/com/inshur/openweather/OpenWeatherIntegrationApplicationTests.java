package com.inshur.openweather;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import com.inshur.openweather.controller.WeatherController;
import com.inshur.openweather.core.CoreRestService;
import com.inshur.openweather.service.WeatherService;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {WeatherController.class, WeatherService.class, CoreRestService.class,RestTemplate.class})
@WebMvcTest
class OpenWeatherIntegrationApplicationTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testLondon() throws Exception {
			String lat ="50.824955973889";
			String lon = "-0.13878781625840952";
	        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/weather-api/getWarmestDayNextSevenDays/" + lat + "/" + lon))
	                .andExpect(status().isOk())
	                .andReturn();
	        String response = result.getResponse().getContentAsString();
	        assertNotNull(response);
    }

	@Test
	public void testNewYork() throws Exception {
			String lat ="40.730610";
			String lon = "-73.935242";
	        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/weather-api/getWarmestDayNextSevenDays/" + lat + "/" + lon))
	                .andExpect(status().isOk())
	                .andReturn();
	        String response = result.getResponse().getContentAsString();
	        assertNotNull(response);
    }

	@Test
	public void testCaracas() throws Exception {
			String lat ="10.500000";
			String lon = "-66.916664";
	        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/weather-api/getWarmestDayNextSevenDays/" + lat + "/" + lon))
	                .andExpect(status().isOk())
	                .andReturn();
	        String response = result.getResponse().getContentAsString();
	        assertNotNull(response);
    }

}
