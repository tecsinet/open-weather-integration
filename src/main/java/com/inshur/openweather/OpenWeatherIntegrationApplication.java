package com.inshur.openweather;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenWeatherIntegrationApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(OpenWeatherIntegrationApplication.class, args);
	}
	
}
