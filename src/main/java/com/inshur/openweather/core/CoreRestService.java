package com.inshur.openweather.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CoreRestService {

	@Autowired
	private RestTemplate restTemplate;

	public <T, Z> ResponseEntity<T> invokeApi(String url, HttpMethod httpMethod, T body,	Class<T> responseClass) {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<T> requestEntity = body != null ? new HttpEntity<>(body, headers) : new HttpEntity<>(headers);
		return restTemplate.exchange(url, httpMethod, requestEntity, responseClass);
	}

}
