package com.inshur.openweather.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class WeatherUtil {
	
	private static final String FORMAT_DATE ="EEEE - MMMM dd yyyy";
	private static final float FAHRENHEIT_BASE = 273.15F;
	private static int MILLISECONDS = 1000;
	public static final String VALUES_TO_EXCLUDE = "hourly,minutely,alerts,current";
	
		
	public static String convertKelvinToCelsius(String temperature) {
		return String.valueOf((int)(Float.parseFloat(temperature) -  FAHRENHEIT_BASE));
	}

	public static String formatUtcDate(String utcDate) {
		Instant  utcInstant = new Date(Long.parseLong(utcDate)*MILLISECONDS).toInstant();
		ZonedDateTime there = ZonedDateTime.ofInstant(utcInstant, ZoneId.of(ZoneOffset.UTC.toString()));
		LocalDateTime formattedDt = there.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
		return DateTimeFormatter.ofPattern(FORMAT_DATE).format(formattedDt);
	}
}
