package com.inshur.openweather.util;

public enum WeatherParametersEnum {
	
	EXCLUDE("exclude"),
	LATITUDE("lat"),
	LONGITUDE("lon");
	
	private String value;
	
	private WeatherParametersEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
