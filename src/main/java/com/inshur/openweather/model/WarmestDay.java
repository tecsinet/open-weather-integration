package com.inshur.openweather.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class WarmestDay {
	private float lat;
	private float lon;
	private String timeZone;
	private String date;
	private String kelvinTemperature;
	private String celsiusTemperature;
	private String humidity;

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public WarmestDay addDate(String date) {
		this.date = date;
		return this;
	}
	public String getCelsiusTemperature() {
		return celsiusTemperature;
	}
	public void setCelsiusTemperature(String celsiusTemperature) {
		this.celsiusTemperature = celsiusTemperature;
	}
	public String getHumidity() {
		return humidity;
	}
	public WarmestDay addCelsiusTemperature(String celsiusTemperature) {
		this.celsiusTemperature = celsiusTemperature;
		return this;
	}
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	public WarmestDay addHumidity(String humidity) {
		this.humidity = humidity;
		return this;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public WarmestDay addLat(float lat) {
		this.lat = lat;
		return this;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}
	public WarmestDay addLon(float lon) {
		this.lon = lon;
		return this;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public WarmestDay addTimeZone(String timeZone) {
		this.timeZone = timeZone;
		return this;
	}
	
	public String getKelvinTemperature() {
		return kelvinTemperature;
	}
	public void setKelvinTemperature(String kelvinTemperature) {
		this.kelvinTemperature = kelvinTemperature;
	}

	public WarmestDay addKelvinTemperature(String kelvinTemperature) {
		this.kelvinTemperature = kelvinTemperature;
		return this;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}
}
