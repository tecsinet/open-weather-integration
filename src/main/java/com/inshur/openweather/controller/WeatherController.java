package com.inshur.openweather.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inshur.openweather.model.WarmestDay;
import com.inshur.openweather.service.WeatherService;

@RestController
@RequestMapping("weather-api")
public class WeatherController {

	@Autowired
	private WeatherService weatherService;
	
	@GetMapping("/getWarmestDayNextSevenDays/{lat}/{lon}")
	WarmestDay one(@PathVariable float lat,@PathVariable float lon) {
	    return weatherService.getWarmestDayNextSevenDays(lat,lon);
	  }

}
