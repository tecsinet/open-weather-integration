package com.inshur.openweather.service;

import static com.inshur.openweather.util.WeatherUtil.VALUES_TO_EXCLUDE;
import static com.inshur.openweather.util.WeatherUtil.convertKelvinToCelsius;
import static com.inshur.openweather.util.WeatherUtil.formatUtcDate;

import java.util.Comparator;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UriComponentsBuilder;

import com.inshur.openweather.core.CoreRestService;
import com.inshur.openweather.model.DayForecast;
import com.inshur.openweather.model.WarmestDay;
import com.inshur.openweather.model.WeatherLocation;
import com.inshur.openweather.util.WeatherParametersEnum;

@Service
public class WeatherService {

	@Value("${openweather.url}")
	private String url;

	@Autowired
	private CoreRestService callRestService;

	public WarmestDay getWarmestDayNextSevenDays(float lat, float lon) {
		DayForecast max = new DayForecast();
		WarmestDay warmestDay = null;

		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url)
				.queryParam(WeatherParametersEnum.EXCLUDE.getValue(), String.valueOf(VALUES_TO_EXCLUDE))
				.queryParam(WeatherParametersEnum.LATITUDE.getValue(), String.valueOf(lat))
				.queryParam(WeatherParametersEnum.LONGITUDE.getValue(), String.valueOf(lon));
		ResponseEntity<WeatherLocation> response = callRestService.invokeApi(uriBuilder.build().toString(),
				HttpMethod.GET, null, WeatherLocation.class);
		WeatherLocation weatherLocation = response.getBody();
		if (weatherLocation != null && !CollectionUtils.isEmpty(weatherLocation.getDaily())) {
			Comparator<DayForecast> comparatorByMaxTemperature = Comparator.comparing(l -> l.getTemp().getDay());
			Comparator<DayForecast> comparatorByHumidity = comparatorByMaxTemperature
					.thenComparing(Comparator.comparingInt(DayForecast::getHumidity).reversed());
			max = weatherLocation.getDaily().stream()
					.max(comparatorByHumidity)
					.orElseThrow(NoSuchElementException::new);
			warmestDay = fillWarmestDay(max,weatherLocation);
		}
		return warmestDay;
	}
	
	private WarmestDay fillWarmestDay(DayForecast dayForeCast,WeatherLocation weatherLocation) {
		WarmestDay warmestDay = new WarmestDay();
		
		warmestDay.addDate(formatUtcDate(dayForeCast.getDt()))
				.addCelsiusTemperature(convertKelvinToCelsius(dayForeCast.getTemp().getDay()))
				.addHumidity(String.valueOf(dayForeCast.getHumidity()))
				.addLat(weatherLocation.getLat())
				.addLon(weatherLocation.getLon())
				.addTimeZone(weatherLocation.getTimezone())
				.addKelvinTemperature(dayForeCast.getTemp().getDay());
		
		return warmestDay;
	}

}
